


'''
	Caution, this removes the modules_pip, before installing the new ones.
'''

import rich
import tomllib
import semver
import toml

import os

import pathlib
from os.path import dirname, join, normpath
import sys
	
this_directory = pathlib.Path (__file__).parent.resolve ()	
pyproject_path = normpath (join (this_directory, "pyproject.toml"))
pip_modules_path = normpath (join (this_directory, "structures/decor_pip"))

'''

'''
def install_dependencies (
	bump = "patch"
):
	os.system (f"rm -rf '{ pip_modules_path }'")

	with open (pyproject_path, "rb") as f:
		data = tomllib.load (f)
					
	dependencies = data ["project"] ["dependencies"]
	
	print ("dependencies:", dependencies)
	
	dependencies_list = list (map (lambda dependency : "'" + dependency + "'", dependencies))
	
	print (dependencies_list)
	
	script = "pip install"
	for dependency in dependencies:
		script += " '" + dependency + "'"
	
	script += f" -t '{ pip_modules_path }'"
	
	
	print (script)
	
	os.system (script)
	
	
install_dependencies ()