

'''
	https://coverage.readthedocs.io/en/latest/api_coverage.html#coverage.Coverage.combine
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../decor_pip',
	'../../decor'
])

'''
	/DB
		/1
		/2
		/3
		/4
		/5	
		index.JSON
'''


import this_module
	

def generate_coverage (
	start = None,
	directory = "",
	data_file = ""
):
	import coverage
	cov = coverage.Coverage (data_file = data_file)
	cov.start ()

	# .. call your code ..

	start ()

	cov.stop ()
	cov.save ()

	#cov.html_report ()
	cov.html_report (directory = directory)

def combine (
	data_files
):
	import coverage
	cov = coverage.Coverage ()
	#cov.start ()
	cov.combine (
		data_files,
		strict = True,
		keep = False
	)
	#cov.save()
	
	cov.save ()
	cov.html_report (directory = "combined")

def combine_2 ():
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	str (normpath (join (this_folder, "coverage_report_1")))

	import coverage
	cov = coverage.Coverage ()

	cov.load ("coverage_report_1")
	cov.load ("coverage_report_2")
	
	cov.combine()
	# Generate a new coverage report
	cov.save()
	# Optionally, generate a report in a specific format (e.g., HTML)
	cov.html_report ()



	

generate_coverage (
	start = this_module.start,
	directory = "coverage_report_1",
	data_file = ".coverage_df_1"
)
generate_coverage (
	start = this_module.start,
	directory = "coverage_report_2",
	data_file = ".coverage_df_2"
)

combine (
	data_files = [
		".coverage_df_1",
		".coverage_df_2"
	]
)

'''
	(cd combined && python3 -m http.server 9000)
'''